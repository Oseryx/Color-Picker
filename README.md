# Color-Picker
 Just the basics of a color picker and nearly it'll be a complete color picker and will be also available as an extension 
 and fully customizable
 
 ## Features that will be added:
 1. Color saving
 2. different layouts of the color picker
 3. Add hex calculations
 4. pick color inside another website

 ## Fixed bugs
 1. Color field cursor
 2. Design errors
