'use strict';

let hiddenCanvas = document.getElementById("color-picker");
let hiddentContext = hiddenCanvas.getContext("2d");
let gradient = hiddentContext.createLinearGradient(0, 0, hiddenCanvas.width, 0);

let canvas = document.getElementById("preview");
let context = canvas.getContext("2d");

canvas.width = 255;
canvas.height = 150;

let circleX = canvas.width - 5, circleY = 5;
let circleExist = false;
let root = document.documentElement;
let chosedColor;
let r, g, b, a = document.getElementById("alpha").value;
let data = hiddentContext.getImageData(0, 0, hiddenCanvas.width, hiddenCanvas.height).data;

let hasClickOnColorSelector = false;
let hasClickOnField = false;

gradient.addColorStop(0, "rgb(255, 0, 0)");
gradient.addColorStop(0.15, "rgb(255, 0, 255)");
gradient.addColorStop(0.33, "rgb(0, 0, 255)");
gradient.addColorStop(0.49, "rgb(0, 255, 255)");
gradient.addColorStop(0.67, "rgb(0, 255, 0)");
gradient.addColorStop(0.84, "rgb(255, 255, 0)");
gradient.addColorStop(1, "rgb(255, 0, 0)");
hiddentContext.fillStyle = gradient;
hiddentContext.fillRect(0, 0, hiddenCanvas.width, hiddenCanvas.height);


drawSwatch(data[0], data[1], data[2]);
let rgba = rgbaToHex(chosedColor[0], chosedColor[1], chosedColor[2], a);

root.style.setProperty('--chosed-color', "rgb(" + chosedColor[0] + "," + chosedColor[1] + "," + chosedColor[2] + "," + a + ")");

document.getElementById("preview").addEventListener("mousedown", (e) => {
    hasClickOnField = true;
    let bounds = document.getElementById("preview").getBoundingClientRect();
    let x = e.clientX - bounds.left;
    let y = e.clientY - bounds.top;

    circleX = x;
    circleY = y;

    root.style.setProperty('--chosed-color', "rgba(" + chosedColor[0] + "," + chosedColor[1] + "," + chosedColor[2] + "," + a + ")");
});
document.getElementById("preview").addEventListener("mouseup", (e) => {
    hasClickOnField = false;
});
document.addEventListener("mouseup", () => {
    if (hasClickOnField) hasClickOnField = false;
    if (hasClickOnColorSelector) hasClickOnColorSelector = false;
});
document.addEventListener("mousemove", (e) => {
    if (hasClickOnField) {
        let bounds = document.getElementById("preview").getBoundingClientRect();
        let x = e.clientX - bounds.left;
        let y = e.clientY - bounds.top;

        if (x > canvas.width) {
            circleX = canvas.width;
        }
        else if (x < 0) {
            circleX = 0;
        }
        else {
            circleX = x;
        }

        if (y > canvas.height) {
            circleY = canvas.height;
        }
        else if (y < 0) {
            circleY = 0;
        }
        else {
            circleY = y;
        }

        drawSwatch(data[0], data[1], data[2]);

        root.style.setProperty('--chosed-color', "rgba(" + chosedColor[0] + "," + chosedColor[1] + "," + chosedColor[2] + "," + a / 100 + ")");
    }
    else if (hasClickOnColorSelector) {
        let bounds = document.getElementsByClassName("range")[0].getBoundingClientRect();
        let x = e.clientX - bounds.left;

        hasClickOnColorSelector = true;
        if (x > hiddenCanvas.width) {
            document.getElementById("selector").style.setProperty('left', hiddenCanvas.width - 7.5 + "px");
        }
        else if (x < 0) {
            document.getElementById("selector").style.setProperty('left', 0 - 7.5 + "px");
        }
        else {
            document.getElementById("selector").style.setProperty('left', x - 7.5 + "px");
        }

        data = hiddentContext.getImageData(x, 0, hiddenCanvas.width, hiddenCanvas.height).data;
        drawSwatch(data[0], data[1], data[2]);

        root.style.setProperty('--chosed-color', "rgba(" + chosedColor[0] + "," + chosedColor[1] + "," + chosedColor[2] + "," + a / 100 + ")");
    }
});
document.getElementsByClassName("range")[0].addEventListener("mousedown", function (e) {
    hasClickOnColorSelector = true;

    document.getElementById("selector").style.setProperty('left', x + "px");
});
document.getElementsByClassName("range")[0].addEventListener("mousemove", (e) => {
    data = hiddentContext.getImageData(x, 0, hiddenCanvas.width, hiddenCanvas.height).data;
    drawSwatch(data[0], data[1], data[2]);
});
document.getElementsByClassName("range")[0].addEventListener("mouseup", () => {
    hasClickOnColorSelector = false;
});
document.getElementById("alpha").addEventListener('input', () => {
    a = document.getElementById("alpha").value;
    document.getElementById("transparency").innerHTML = `Transparency: ` + a / 100;
    root.style.setProperty('--chosed-color', "rgba(" + chosedColor[0] + "," + chosedColor[1] + "," + chosedColor[2] + "," + a / 100 + ")");
});

function drawSwatch(r, g, b) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    let col = rgbToHLS(r, g, b);
    let gradB = context.createLinearGradient(0, 0, 0, canvas.height);
    gradB.addColorStop(0, "white");
    gradB.addColorStop(1, "black");
    let gradC = context.createLinearGradient(0, 0, canvas.width, 0);
    gradC.addColorStop(0, `hsla(${Math.floor(col.hue)},100%,50%,0)`);
    gradC.addColorStop(1, `hsla(${Math.floor(col.hue)},100%,50%,1)`);

    context.fillStyle = gradB;
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = gradC;
    context.globalCompositeOperation = "multiply";
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.globalCompositeOperation = "source-over";
    drawPicker(circleX, circleY);
}
function drawPicker(x, y) {
    context.beginPath();
    context.strokeStyle = (y < 100 && x < 100) ? "#000" : "#fff";
    context.arc(x, y, 5, 0, 2 * Math.PI);
    hiddentContext.translate(x, y);
    context.stroke();
    chosedColor = context.getImageData(x, y, 1, 1).data;
}
function rgbToHLS(red, green, blue, result = {}) {
    var hue, sat, lum, min, max, dif, r, g, b;
    r = red / 255;
    g = green / 255;
    b = blue / 255;
    min = Math.min(r, g, b);
    max = Math.max(r, g, b);
    lum = (min + max) / 2;
    if (min === max) {
        hue = 0;
        sat = 0;
    } else {
        dif = max - min;
        sat = lum > 0.5 ? dif / (2 - max - min) : dif / (max + min);
        switch (max) {
            case r:
                hue = (g - b) / dif;
                break;
            case g:
                hue = 2 + ((b - r) / dif);
                break;
            case b:
                hue = 4 + ((r - g) / dif);
                break;
        }
        hue *= 60;
        if (hue < 0) {
            hue += 360;
        }
    }
    result.lum = lum * 255;
    result.sat = sat * 255;
    result.hue = hue;
    return result;
}
function rgbaToHex(r, g, b, a) {
    const rgba = [r, g, b, a];
    let hex = "";

    for (let value of [r, g, b]) {
        if (value === a) {
            value = Math.floor((a / 100) * 255);
        }
        let h = Number(value).toString(16);
        hex += (h < 2) ? "0" + h : h;
    }
    return hex;
}
function hexToRGB(hex, alpha) {
    let r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);
    if (alpha) {
        return "rgba(" + r + ", " + g + ", " + b + ", " + Number((parseInt(alpha, 16) / 255).toFixed(2)) + ")";
    }
    return "rgb(" + r + ", " + g + ", " + b + ")";
}
function hexToRgbA(hex, alpha) {
    var c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length == 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x' + c.join('');
        return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
    }
}
